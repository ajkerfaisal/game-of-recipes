//
//  Game_of_RecipesTests.swift
//  Game of Recipes
//
//  Created by Mohammad.Faisal on 8/16/17.
//  Copyright © 2017 Faisal LLC. All rights reserved.
//

import XCTest
@testable import Game_of_Recipes

class Game_of_RecipesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testRecipeData(){
        APIClient.sharedInstance.fetchRecipes { (dataSource, err) in
            XCTAssertNil(err)
            XCTAssertNotNil(dataSource)
        }
    }
}
