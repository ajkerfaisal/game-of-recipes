# README #

### What is this repository for? ###

* This app is created as skill demonstration for affinitas GmbH	
* Version: 0.0.1

### How do I get set up? ###

* pod install
		
### Dependencies ###
* TRON
* SwiftyJSON
* Cosmos
* SDWebImage
* SCLAlertView
* SVProgressHUD

### Who do I talk to? ###

* Mohammad Faisal
* ajkerfaisal@yahoo.com
