//
//  RecipesViewController.swift
//  Games of Recipes
//
//  Created by Mohammad.Faisal on 8/7/17.
//  Copyright © 2017 No Company LLC. All rights reserved.
//

import UIKit
import TRON
import SVProgressHUD

class RecipesViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var datasource: RecipeDataSource?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.tableView.separatorColor = UIColor.clear
        
        //fetch recipes from web-services
        loadRecipes()
    }
    
    public func loadRecipes(){
        CentralOperator.sharedInstance.showProgressHUD(message: "")
        
        //fetch recipes from web-services
        APIClient.sharedInstance.fetchRecipes { (dataSource, err) in
            
            //recipes response received
            if let err = err {
                if let apiError = err as? APIError<APIClient.JSONError> {
                    if apiError.response?.statusCode != 200{
                        CentralOperator.sharedInstance.showAlertView(message: "Error loading data")
                    }
                }
                return
            }
            
            //set data members and reload tableview
            self.datasource = dataSource
            self.tableView.reloadData()
            CentralOperator.sharedInstance.dismissProgressHUD()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func loginAction(_ sender: Any) {
        self.performSegue(withIdentifier: "loginView", sender: self)
    }

    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let recipe = self.datasource?.recipes[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.recipe = recipe
            }
        }
    }
}

//table cell delegates
extension RecipesViewController : RecipeCellDelegate{
    
    func didTapFavorite(recipe_id: String) {
        
        if CentralOperator.sharedInstance.currentuser.email != nil{
            let params: [String : Any] = ["recipe_id": recipe_id, "user_id" : CentralOperator.sharedInstance.currentuser.email!]
            APIClient.sharedInstance.toggleFavorite(params:params, completion: { (responseData, err) in
                if responseData != nil{
                    CentralOperator.sharedInstance.dismissProgressHUD()
                    if responseData?.response.code == Constrains.codeSuccess {
                        // now reload cell view
                    }else{
                        // when code is not success
                        if let text = responseData?.response.message{
                            CentralOperator.sharedInstance.showAlertView(message: text)
                        }
                    }
                }
                else{
                    CentralOperator.sharedInstance.showAlertView(message: "Error communicating with server")
                }
            })
        }else{
            
            //user is not logged in. show him/her login page
            self.performSegue(withIdentifier: "loginView", sender: self)
        }
    }
}

//table-view delegates
extension RecipesViewController{
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.datasource?.recipes.count{
            return count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecipeTableViewCell
        cell.recipe = self.datasource?.recipes[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetails", sender: self)
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}

