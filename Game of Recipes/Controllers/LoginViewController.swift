//
//  LoginViewController.swift
//  Game of Recipes
//
//  Created by Mohammad.Faisal on 8/12/17.
//  Copyright © 2017 No Company LLC. All rights reserved.
//

import UIKit
import TRON

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //on tap view white area, hide soft keyboard
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        //client side validation
        if !CentralOperator.sharedInstance.isValidEmail(testStr: emailTextField.text!){
            CentralOperator.sharedInstance.showAlertView(message: "Please enter valid email")
        }else if passwordTextField.text!.isEmpty{
            CentralOperator.sharedInstance.showAlertView(message: "Please enter password")
        }else{
            //on successful client side validation
            APIClient.sharedInstance.checkLogin(email: emailTextField.text!, password: passwordTextField.text!, completion: { (responseData, err) in
                
                if responseData != nil{
                    CentralOperator.sharedInstance.dismissProgressHUD()
                    if responseData?.response.code == Constrains.codeSuccess {
                        CentralOperator.sharedInstance.showAlertView(message: "Login successful")
                    }else{
                        // when code is not success
                        if let text = responseData?.response.message{
                            CentralOperator.sharedInstance.showAlertView(message: text)
                        }
                    }
                }
                else{
                    CentralOperator.sharedInstance.showAlertView(message: "Error communicating with server")
                }
            })
        }
    }

    @IBAction func forgotPasswordAction(_ sender: Any) {
        CentralOperator.sharedInstance.showAlertView(message: "This feature is not available yet")
    }
    
    @IBAction func signupAction(_ sender: Any) {
        CentralOperator.sharedInstance.showAlertView(message: "This feature is not available yet")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
