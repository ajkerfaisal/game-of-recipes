//
//  DetailViewController.swift
//  Games of Recipes
//
//  Created by Mohammad.Faisal on 8/7/17.
//  Copyright © 2017 No Company LLC. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        if recipe != nil {
            if let label = detailDescriptionLabel {
                label.text = recipe?.name
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var recipe: Recipe? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

