//
//  RecipeTableViewCell.swift
//  Games of Recipes
//
//  Created by Mohammad.Faisal on 8/7/17.
//  Copyright © 2017 No Company LLC. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

protocol RecipeCellDelegate {
    func didTapFavorite(recipe_id: String)
}

class RecipeTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var carbsLabel: UILabel!
    @IBOutlet weak var fatsLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var ratingCosmosView: CosmosView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var ratingCountLabel: UILabel!
    
    var delegate : RecipeCellDelegate?
    var recipe : Recipe!{
        didSet{
            self.updateUI()
        }
    }
    
    func updateUI(){
        nameLabel.text = recipe.name
        detailsLabel.text = recipe.details
        thumbImageView.sd_setImage(with: URL(string: recipe.thumb_url!),
                                   placeholderImage: UIImage(color: UIColor.randomColor()))
        caloriesLabel.text = recipe.calories
        carbsLabel.text = recipe.carbs
        fatsLabel.text = recipe.fats
        likesLabel.text = "\(String(describing: recipe.favorites!)) Likes"
        authorLabel.text = "by \(String(describing: recipe.user!.name!))"
        if recipe.rating != nil{
            ratingCosmosView.rating = recipe.rating!
            ratingCountLabel.text = "\(String(describing: recipe.ratings_count!))"
        }else{
            ratingCosmosView.rating = 0;
            ratingCountLabel.text = "0"
        }
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        delegate?.didTapFavorite(recipe_id: recipe.id!)
    }
}
