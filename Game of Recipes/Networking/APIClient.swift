//
//  API.swift
//  Game of Recipes
//
//  Created by Mohammad.Faisal on 8/13/17.
//  Copyright © 2017 Faisal LLC. All rights reserved.
//

import Foundation
import TRON
import SwiftyJSON

struct APIClient {
    
    let tron = TRON(baseURL: "https://www.gameofrecipes.com/api") //base url
    
    static let sharedInstance = APIClient()
    
    func fetchRecipes(completion: @escaping (RecipeDataSource?, Error?) -> ()) {
        
        //create request for endpoint
        let request: APIRequest<RecipeDataSource, JSONError> = tron.request("/recipes")
        request.perform(withSuccess: { (dataSource) in
            completion(dataSource, nil)
        }) { (err) in
            //when failed to load recipes from web-services, read from bundle
            completion(self.fetchAllRecipesFromResource(), nil)
        }
    }
    
    func fetchAllRecipesFromResource() -> RecipeDataSource{
        
        var json : JSON?
        do {
            if let file = Bundle.main.url(forResource: "recipes", withExtension: "json") {
                let data = try Data(contentsOf: file)
                json = JSON(data)
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return try! RecipeDataSource(json: json!)
    }
    
    func toggleFavorite(params: [String: Any], completion: @escaping (ResponseData?, Error?) -> ()){
        let request: APIRequest<ResponseData, JSONError> = tron.request("/toggleFavorite")
        request.method = .post
        request.parameters = params
        request.perform(withSuccess: { (dataSource) in
            completion(dataSource, nil)
        }) { (err) in
            completion(nil, nil)
        }
    }
    
    func checkLogin(email: String, password: String, completion: @escaping (ResponseData?, Error?) -> ()){
        let request: APIRequest<ResponseData, JSONError> = tron.request("/login")
        request.perform(withSuccess: { (dataSource) in
            completion(dataSource, nil)
        }) { (err) in
            completion(nil, nil)
        }
    }
    
    class JSONError: JSONDecodable {
        required init(json: JSON) throws {
            print("JSON ERROR")
        }
    }
}
