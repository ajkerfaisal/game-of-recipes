//
//  CentralOperator.swift
//  Games of Recipes
//
//  Created by Mohammad.Faisal on 8/8/17.
//  Copyright © 2017 No Company LLC. All rights reserved.
//

import UIKit
import SCLAlertView
import SVProgressHUD

class CentralOperator {
    
    static let sharedInstance = CentralOperator()
    
    var currentuser = User();
    
    private init() {
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        //return boolean after validating input email string
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func showAlertView (title: String , message: String){
        
        SCLAlertView().showInfo(title, subTitle: message)
    }
    
    func showAlertView (message: String){
        
        SCLAlertView().showInfo(NSLocalizedString("AppName", comment: ""), subTitle: message)
    }
    
    func showAlertView (alertType: Int, message: String){
        
        //TODO: Implement for alert type: general, warning, info, error
    }
    
    func showProgressHUD(message: String){
        if message.isEmpty {
            SVProgressHUD.show()
        }else{
            SVProgressHUD.showInfo(withStatus: message)
        }
        
    }
    
    func dismissProgressHUD() {
        SVProgressHUD.dismiss()
    }
}
