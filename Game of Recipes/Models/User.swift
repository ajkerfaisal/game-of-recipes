//
//  User.swift
//  Games of Recipes
//
//  Created by Mohammad.Faisal on 8/7/17.
//  Copyright © 2017 No Company LLC. All rights reserved.
//

import UIKit
import SwiftyJSON

public class User{
    
    var name : String?
    var latlong : String?
    var email : String?
    
    init(name: String, latlong: String, email: String) {
        self.name = name
        self.latlong = latlong
        self.email = email
    }
    
    init(){
    }
}
