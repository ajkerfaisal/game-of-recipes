//
//  Response.swift
//  Game of Recipes
//
//  Created by Mohammad.Faisal on 8/13/17.
//  Copyright © 2017 Faisal LLC. All rights reserved.
//

import SwiftyJSON

public class Response{

    var code : Int?
    var message : String?
    
    init(jsonObject : JSON) {
        self.code = jsonObject["code"].int
        self.message = jsonObject["message"].string
    }

}
