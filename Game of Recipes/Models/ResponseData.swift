//
//  ResponseData.swift
//  Game of Recipes
//
//  Created by Mohammad.Faisal on 8/13/17.
//  Copyright © 2017 Faisal LLC. All rights reserved.
//

import SwiftyJSON
import TRON

public class ResponseData : JSONDecodable {
    
    var response: Response
    
    public required init(json: JSON) throws {
        guard json != JSON.null else {
            throw NSError(domain: "game-of-recipes.com", code: 1, userInfo: [NSLocalizedDescriptionKey: "Parsing JSON was not valid."])
        }
        self.response = Response(jsonObject: json)
    }
}
