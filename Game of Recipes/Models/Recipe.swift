//
//  Recipe.swift
//  Games of Recipes
//
//  Created by Mohammad.Faisal on 8/7/17.
//  Copyright © 2017 No Company LLC. All rights reserved.
//

import SwiftyJSON

class Recipe: NSObject {
    
    var id : String?
    var name : String?
    var headline : String?
    var details: String?
    var country : String?
    var ingredients : [Any]?
    var ingredients_deliverable : [Any]?
    var ingredients_undeliverable : [Any]?
    var difficulty : Int?
    var favorites : Int?
    var image_url : String?
    var thumb_url : String?
    var rating : Double?
    var ratings_count : Int?
    var time : String?
    var calories : String?
    var carbs : String?
    var fats : String?
    var proteins : String?
    var fibers : String?
    var user : User?
    
    
    init(jsonObject : JSON) {
        
        //TODO: Fetching all properties of recipe will be two-step process. 1. RecipesList & 2. RecipeDetails  
        
        self.id = jsonObject["id"].string
        self.name = jsonObject["name"].string
        self.headline = jsonObject["headline"].string
        self.details = jsonObject["description"].string
        self.country = jsonObject["id"].string
        self.ingredients = jsonObject["ingredients"].arrayObject
        self.ingredients_deliverable = jsonObject["deliverable_ingredients"].arrayObject
        self.ingredients_undeliverable = jsonObject["undeliverable_ingredients"].arrayObject
        self.difficulty = jsonObject["difficulty"].int
        self.favorites = jsonObject["favorites"].int
        self.image_url = jsonObject["image"].string
        self.thumb_url = jsonObject["thumb"].string
        self.rating = jsonObject["rating"].double
        self.ratings_count = jsonObject["ratings"].int
        self.time = jsonObject["time"].string
        self.calories = jsonObject["calories"].string
        self.carbs = jsonObject["carbos"].string
        self.fats = jsonObject["fats"].string
        self.proteins = jsonObject["proteins"].string
        self.fibers = jsonObject["fibers"].string
        self.user = User(name: (jsonObject["user"]["name"].string)!, latlong:(jsonObject["user"]["latlng"].string)!, email: (jsonObject["user"]["email"].string)!)
        
    }
}
