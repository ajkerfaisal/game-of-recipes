//
//  RecipeDataSource.swift
//  Game of Recipes
//
//  Created by Mohammad.Faisal on 8/13/17.
//  Copyright © 2017 Faisal LLC. All rights reserved.
//

import TRON
import SwiftyJSON

class RecipeDataSource: JSONDecodable {
    
    var recipes: [Recipe]

    required init(json: JSON) throws {
        guard let recipesJsonArray = json.array else {
            throw NSError(domain: "game-of-recipes.com", code: 1, userInfo: [NSLocalizedDescriptionKey: "Parsing JSON was not valid."])
        }
        self.recipes = recipesJsonArray.map{Recipe(jsonObject: $0)}
    }
    
    required init(recipes: [Recipe]){
        self.recipes = recipes;
    }
}
